import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class Test {

    public static void main(String[] args) throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
        server.createContext("/test", new MyHandler());
        server.setExecutor(null); // creates a default executor
        server.start();
    }

    private static String listUsers() {
    	try {
             Process p = Runtime.getRuntime().exec("cmd /C query user /server:localhost");
             BufferedReader in = new BufferedReader(
                                 new InputStreamReader(p.getInputStream()));
             String text = "";
             String line;
             while ((line = in.readLine()) != null) {
                 text += ("\n " + line);
             }
             Process p2 = Runtime.getRuntime().exec("cmd /C netstat -nf | find \"3389\"");
             BufferedReader in2 = new BufferedReader(
                                 new InputStreamReader(p2.getInputStream()));
             while ((line = in2.readLine()) != null) {
                 text += ("\n " + line);
             }
             return text;
         } catch (IOException e) {
             e.printStackTrace();
         }
		return "Não foi possível executar";
    }
    
    static class MyHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = listUsers();
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }
    
}
